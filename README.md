# Test Coverage Visualizer (TCV)

# Feature branches
* Master branch is the central repository and each new feature needs to be implemented in a branch and then merged into master.  
* Branches are named after the new feature that is worked on in that branch.  
* Feature branches are merged into master when the work for current sprint is completed.  

# Merge requests
* New feature should be done with a MR started no later than the wednesday on the last week of the sprint.
* Review the MR before the weekend on the last week of the sprint.
* Merge request approved and executed no later than tuesday in the first week of the new sprint.
* Two developers that haven't had any commit activity related to code in MR reviews and approves it. When starting the MR, assign the desired reviewers.
* When accepting the MR we squash the commits of that feature branch

# Documentation
* Documents also gets merged into master using a branch. Create a branch for corresponding document, push pdf file and start merge request.
* Within the MR give link to the overleaf repository for reviewers. If changes were necessary simply push the new pdf into branch and @ the reviewers to approve the MR.

# Testing submodule
A git submodule is used to run all tests in a separate repository. Below are some instructions for using this module.
* Initiating the submodule after pulling submodule changes:
```
git submodule init
git submodule update        #Permission might be denied when you use some editors, if this happens use Git Bash.
```
* Pulling changes made according to the .gitmodules:
```
git submodule update --remote
```
This will make git go into all submodules and fetch & update them with current checkout
* Writing changes on the submodule:
Make the changes within the Testing Module directory
```
git commit -m "your message"
git push
cd ..       #Changing directory to main
git commit -m "your message"
git push
```
Now when others pull your changes they will see an updated .gitmodules. This will allow you to update your Testing Module directory with the changes made by following instructions for pulling changes described above.
* Other useful commands:
```
git push --recurse-submodules=check         #Fails if commits on submodules are not pushed when you push main repository
git push --recurse-submodules=on-demand     #Git goes into submodules and pushes them before pushing main repository.

```

# Installation
## Parser
To set up the parser run the command below from the directory Code/parser.  
```
pip install -r requirements.txt
```
## Frontend
### Project setup
Install Node.js, then open the frontend repo and run:
```
npm install
```
### Compiles and hot-reloads for development
```
npm run serve
```
### Compiles and minifies for production
```
npm run build
```
### Lints and fixes files
```
npm run lint
```
### Style Guide
The project is trying to follow the [Vue Style Guide](https://vuejs.org/v2/style-guide/#Component-style-scoping-essential)
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
 
## Backend
### Dependencies
If you are using Linux, begin by running the command below.
```
sudo apt-get install python3.6-dev default-libmysqlclient-dev build-essential
```
To set up the backend run the command below from the directory Code/databas.
```
pip install -r requirements.txt
```
### Database installation
If your OS is Linux and you don't already have a database, follows steps below.
```
sudo apt-get install mariadb-server
sudo mysql-secure-installation
```
* Assign 'root' password so you can access this database. Can be left empty.
* Use port 3306.          # This is set in Code/databas/database_handler/settings.py on variable DATABASES, if you install using some other port simply change this there.

If your OS is Windows and you don't have a database for the application, follow the steps below.
* Download mariadb server 10.5.9
* Assign 'root' password so you can access this database. Can be left empty.
* Use port 3306.          # This is set in Code/databas/database_handler/settings.py on variable DATABASES, if you install using some other port simply change this there.

If you already have a database, change contents of DATABASES in the file settings.py found in Code/databas/database_handler/settings.py accordingly.  

### Database setup
Setup the database in Windows using steps below.
* run mysqlclient found in the installed folder of mariadb.  # This step requires root password which is set during installation of the database.
```
CREATE DATABASE TCVdb;          # TCVdb is set in Code/databas/database_handler/settings.py on variable DATABASES, if you want to use some other name simply change this.
use TCVdb;
CREATE USER TCVuser@localhost IDENTIFIED BY '1234';               # User name & User password is set in the variable DATABASES within Code/databas/database_handler/settings.py, if you want to use some other name or password simply change this there.
GRANT ALL PRIVILEGES ON TCVdb.* TO TCVuser@localhost IDENTIFIED BY '1234';   
```
Setup the database in Linux using steps below.
```
mysql -u root  # This step requires root password, set during installation of the database.
CREATE DATABASE TCVdb;          # TCVdb is set in Code/databas/database_handler/settings.py on variable DATABASES, if you want to use some other name simply change this.
use TCVdb;
CREATE USER TCVuser@localhost IDENTIFIED BY '1234';               # User name & User password is set in the variable DATABASES within Code/databas/database_handler/settings.py, if you want to use some other name or password simply change this there.
GRANT ALL PRIVILEGES ON TCVdb.* TO TCVuser@localhost IDENTIFIED BY '1234';   
```
### Run backend
Migrate models to the database with manage.py inside Code/databas/database_handler/ according to the steps below .
```
python manage.py makemigrations
python manage.py migrate
```
Start the backend by moving to folder Code/databas/database_handler/ and running the command below.
```
python manage.py runserver
```
### Useful commands
```
python manage.py flush          # Removes all contents from database
```
## Apache

This installation guide assumes that Ubuntu is used. Other distributions may require different installation commands, may result in a different file structure and may use different commands to operate the http server.

This guide also assumes that all other parts of the project (parser, backend and frontend) are installed correctly. It is highly recommended that you have confirmed that they work as intended before proceeding with installing apache2. 

Let’s begin with updating your system (this is optional). Then we install apache2 and mod_wsgi. 

```
sudo apt-get update
sudo apt-get install apache2
sudo apt-get install libapache2-mod-wsgi-py3
```

Move to this folder.

```
cd /etc/apache2/sites-available/
```

Create two config files, name them frontend.conf, backend.conf respectively (you can choose different names).

```
touch backend.conf
touch frontend.conf
```

Add the following text to the backend.conf file:

```
<VirtualHost 192.168.1.34:1600> 
ServerName tcv-backend 
DocumentRoot /home/ellesmera/code-coverage-visualization-tool/Code/databas/database_handler 
ErrorLog ${APACHE_LOG_DIR}/error.log 
CustomLog ${APACHE_LOG_DIR}/access.log combined 

<Directory /home/ellesmera/code-coverage-visualization-tool/Code/databas/database_handler/database_handler> 
<Files wsgi.py> 
Require all granted 
</Files> 
</Directory> 

WSGIScriptAlias / /home/ellesmera/code-coverage-visualization-tool/Code/databas/database_handler/database_handler/wsgi.py 

WSGIDaemonProcess tcv python-path=/home/ellesmera/code-coverage-visualization-tool/Code/databas/database_handler python-home=/home/ellesmera/code-coverage-visualization-tool/Code/tcv_env 

WSGIProcessGroup tcv 
</VirtualHost>
```

Add the following text to the frontend.conf file: 

```
<VirtualHost 192.168.1.34:1500> 
ServerName tcv-frontend 
DocumentRoot /home/ellesmera/code-coverage-visualization-tool/Code/tcv-frontend/dist 
ErrorLog ${APACHE_LOG_DIR}/error.log 
CustomLog ${APACHE_LOG_DIR}/access.log combined 

<Directory /home/ellesmera/code-coverage-visualization-tool/Code/tcv-frontend/dist> 
Require all granted 
FallbackResource /index.html
</Directory> 
</VirtualHost>
```

Save (this may require password). 

**IMPORTANT**: You need to **change the paths** used in the two conf files to match where the project and the python environment are installed on your system. Read through **all** lines containing a path and change them accordingly, otherwise the server will not start. You also need to **change the ip address** to your local ip address (you could also change the port if desired, but not required).

Next, you need to enable the virtual host files for the django project. 

```
sudo a2ensite backend.conf
sudo a2ensite frontend.conf
```

Optional: Disable the default virtual host file.

```
sudo 000-default.conf
```

Open /etc/apache2/ports.conf and remove “Listen 80” and replace it with the ip addresses and port numbers you used in the two virtual host respectively, example: 

```
Listen 192.xxx.x.xx:1500
Listen 192.xxx.x.xx:1600
```

Let’s confirm there are no syntax errors within the newly added configuration files.

```
apache2ctl configtest
```

Verify that the syntax is OK. If you get the error message AH00558 (Could not fully determine the server's fully qualified domain name …) ignore that for now. 

Now, open database_handler/database_handler/settings.py. Add the ip address you used for the virtual host in backend.conf to the ALLOWED_HOST list: 

```
ALLOWED_HOSTS = [..., “192.xxx.x.xx”]
```
Within the same file, add the ip address and the corresponding port you used for the virtual host in frontend.conf to the CORS_ALLOWED_ORIGINS list: 

```
CORS_ALLOWED_ORIGINS = [..., “http://192.xxx.x.xx:1500”]
```

Then, open the parser config file. Change the url and port to be the same as in backend.conf. 

```
url = “192.xxx.x.xx”
port = 1600
```

Lastly, open tcv-frontend/.env. Change the url to the backend url as such:

```
VUE_APP_API_ENDPOINT = “http://192.xxx.x.xx:1600/rest_api”
```

# Starting the application
* For frontend, go to tcv-frontend and run:
```
npm run serve
```

* For backend, go to databas/database_handler:
```
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```

* for getting started with the parser, go to Code/parser and run one of the following help commands:
```
python tcv.py -h
```
```
python tcv.py add -h
```
```
python tcv.py parse -h
```

* for apache:

First, make sure that both the frontend and the backend are up to date

```
python3 manage.py makemigrations api
python3 manage.py migrate
npm run build
```

**IMPORTANT**: Remember to redo the above steps when changes have been made in order for them to be reflected when using apache. 

Now it’s time to start the server: 

```
sudo service apache2 start
```

Each time new changes have been made the server needs to be restarted: 

```
sudo service apache2 reload
```

If you wish to stop the server, type: 

```
sudo service apache2 stop
```

If at any time you receive any errors, you can most likely find them documented in more detail in /var/log/apache2/error.log. Shortcut: 

```
tail -f /var/log/apache2/error.log
```
