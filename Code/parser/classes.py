class File:
    """
    Keeps track of all information related to a file from a test run.
    """

    def __init__(self, name, hlr, statement, decision, mcdc, CSUs=None):
        """
        Constructor for the File class.

        :param name: Name of the file.
        :param hlr: Home Location Register entry coverage in percentage.
        :param statement: The statement coverage of the file in percentage.
        :param decision: The decision coverage of the file in percentage.
        :param mcdc: The MC / DC coverage of the file in percentage.
        :param CSUs: A list of CSU names which the file is related to.
        """
        self.name = name
        self.hlr = hlr
        self.statement = statement
        self.decision = decision
        self.mcdc = mcdc
        self.CSUs = CSUs if CSUs is not None else ["General"]

    def __hash__(self):
        return hash(self.name)

    def add_CSU(self, CSU):
        """
        Add a CSU to list of CSU:s which the file is related to.CSCI

        :param CSU: The name of CSU to add.
        """
        if "General" in self.CSUs:
            self.CSUs = [CSU]
        else:
            self.CSUs.append(CSU)

    def print(self, depth=0):
        """
        Print class info in a read-friendly way.

        :param depth: The depth of the tabbing to print.
        """
        print('\t' * depth + f"File: {self.name}\n" +
              '\t' * (depth + 1) + f"HLR: {self.hlr}%\n" +
              '\t' * (depth + 1) + f"Statement Coverage: {self.statement}%\n" +
              '\t' * (depth + 1) + f"Decision Coverage: {self.decision}%\n" +
              '\t' * (depth + 1) + f"MC/DC: {self.mcdc}%\n" +
              '\t' * (depth + 1) + f"CSUs: {self.CSUs}")

    def __eq__(self, other):
        return isinstance(other, File) \
               and self.name == other.name \
               and self.hlr == other.hlr \
               and self.statement == other.statement \
               and self.decision == other.decision \
               and self.mcdc == other.mcdc \
               and self.CSUs == other.CSUs

    def __repr__(self):
        output = ""
        for key, value in self.__dict__.items():
            output += f"{key}: {value }"
        return output


class CSCI:
    """
    Contains the data from parsing a CSCI-file.
    """

    def __init__(self, name, files, timestamp, runner):
        """
        Constructor for the CSCI class.

        :param name: The name of the CSCI.
        :param files: The list of File objects that are in the CSCI.
        :param timestamp: The timestamp for which the test was run.
        :param runner: The name of the system that ran the test.
        """
        self.name = name
        self.files = files
        self.timestamp = timestamp
        self.runner = runner

    def print(self, depth=0):
        """
        Print class info in a read-friendly way.

        :param depth: The depth of the tabbing to print.
        """
        print('\t' * depth + f"CSCI: {self.name}\n" +
              '\t' * (depth + 1) + f"Time: {self.timestamp}\n" +
              '\t' * (depth + 1) + f"Runner: {self.runner}")
        for file in self.files:
            file.print(depth + 1)

    def __eq__(self, other):
        return self.name == other.name \
               and set(self.files) == set(other.files) \
               and self.timestamp == other.timestamp \
               and self.runner == other.runner

    def __repr__(self):
        output = ""
        for key, value in self.__dict__.items():
            output += f"{key}: {value }"
        return output


class Summary:
    """
    Contain the summary of a test run.
    """

    def __init__(self, hlr, statement, decision, mcdc):
        """
        Constructor for the Summary class.

        :param hlr: Home Location Register entry coverage in percentage.
        :param statement: The statement coverage of the file in percentage.
        :param decision: The decision coverage of the file in percentage.
        :param mcdc: The MC / DC coverage of the file in percentage.
        """
        self.hlr = hlr
        self.statement = statement
        self.decision = decision
        self.mcdc = mcdc

    def print(self, depth=0):
        """
        Print class info in a read-friendly way.

        :param depth: The depth of the tabbing to print.
        """
        output = '\t' * depth + "Test Run Summary:\n"
        for coverage, title in zip([self.hlr, self.statement, self.decision, self.mcdc],
                            ["HLR Entry", "Statement Coverage", "Decision Coverage", "MC / DC"]):
            if coverage is not None:
                output += '\t' * (depth + 1) + f"{title}: {coverage}%\n"
            else:
                output += '\t' * (depth + 1) + f"{title}: N/A\n"
        print(output)

    def __eq__(self, other):
        return self.hlr == other.hlr and \
               self.statement == other.statement and \
               self.decision == other.decision and \
               self.mcdc == other.mcdc

    def __repr__(self):
        output = ""
        for key, value in self.__dict__.items():
            output += f"{key}: {value }"
        return output


class Result:
    """
    Contains the data of a Test Result found in Result-files.
    """

    def __init__(self, name, passed=0, failed=0):
        """
        Constructor for the Result class.

        :param name: Name of tested file.
        :param passed: The number of passed checks.
        :param failed: The number of failed checks.
        """
        self.name = name
        self.passed = passed
        self.failed = failed

    def print(self, depth=0):
        """
        Print class info in a read-friendly way.

        :param depth: The depth of the tabbing to print.
        """
        print('\t' * depth + f"Result: {self.name}\n" +
              '\t' * (depth + 1) + f"Passed: {self.passed}\n" +
              '\t' * (depth + 1) + f"Failed: {self.failed}")

    def __add__(self, other):
        """
        Operator+, Add passed and failed from rhs to lhs
        """
        return Result(self.name, self.passed + other.passed, self.failed + other.failed)

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        """
        Operator=, Compares the name of this result with another Result or string.

        :param other: Result or string to be compared with self.
        """
        if isinstance(other, str):
            return self.name == other
        return self.name == other.name and self.passed == other.passed and self.failed == other.failed

    def __repr__(self):
        output = ""
        for key, value in self.__dict__.items():
            output += f"{key}: {value }"
        return output
