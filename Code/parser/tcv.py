#!/usr/bin/env python3

from parse import parse_config, parse_CSCI, parse_results, parse_summary
from pathlib import Path
import argparse
import requests

SCRIPT_DIR = Path.cwd()


def construct_parser():
    """
    Construct the subparsers and their arguments.
    """
    parser = argparse.ArgumentParser(
        description="Test Coverage Visualiser (TCV) is a program for parsing "
                    "files generated from Cantata++, Saab CovSum and "
                    "GoogleTest to then store the result in a database.")

    subs = parser.add_subparsers(help="main action to be performed",
                                 dest="command")

    # Parse sub-parser
    sub_parser = subs.add_parser("parse", help="parse and print test "
                                               "results to the terminal")
    add_common_arguments(sub_parser, False)

    # Add sub-parser
    add = subs.add_parser("add", help="parse and add test result to database")
    add_common_arguments(add, True)

    return parser


def post_results(results, test_run_id, url):
    """
    Send a post-request containing a result to the database for each result in results.

    :param results: A list of result-files that are to be sent to the database.
    :param test_run_id: The given id for the test run that included the result.
    :param url: Database url to receive the request.
    """
    test_payload = []
    for test in results:
        test_payload.append({
            "name": test.name,
            "test_run": test_run_id,
            "passed": test.passed,
            "failed": test.failed
        })

    response = requests.post(url + "test/", json=test_payload)


def match_file_with_csu_id(file_payload, csus):
    """
    Exchanges the CSU name with its database id in the file_payload list.

    :param file_payload: A payload of files to post with CSUs name instead of the database id of that CSU.
    :param csus: List of csus from the response that contains the id given by the database.
    """

    CSU_to_id_dict = {CSU["name"]: CSU["id"] for CSU in csus}

    # Uses the id from response to overwrite CSU name with its id.
    file_payload = list(
        map(lambda file: {"name": file["name"], "csus": list(map(lambda CSUs: CSU_to_id_dict[CSUs], file["csus"]))},
            file_payload))
    return file_payload


def match_test_result_with_file_id(files, test_result_payload, csus):
    """
    Exchanges the File name with its database id in the test_result_payload list.

    :param test_result_payload: A payload of test results to post containing the File json representation instead of the database id of that File.
    :param files: List of files from the response that contains the id given by the database.
    :param csus: List of csus from the response that contains the id given by the database.
    """

    CSU_to_id_dict = {CSU["name"]: CSU["id"] for CSU in csus}
    for i in range(len(test_result_payload)):
        test_result = test_result_payload[i]
        CSUs = list(map(lambda CSU: CSU_to_id_dict[CSU], test_result["file"]["csus"]))
        for file in files:

            if (test_result["file"]["name"] == file["name"]) and (CSUs == file["csus"]):
                test_result_payload[i]["file"] = file["id"]
                break
    return test_result_payload


def create_payloads_without_matched_id(sent_CSCI, files, test_run_id):
    """
    Creates the payloads for CSU, File and Test result without the database id for CSUs and Files. 

    :param sent_CSCI: This is the id of the CSCI that was given by the database upon posting it.
    :param files: List of parsed files belongs to the CSCI that have been posted to database.
    :param test_run_id: The id of the test run given by the database.
    """

    sent_CSUs = []
    sent_files = []
    file_payload = []
    CSU_payload = []
    test_result_payload = []

    for file in files:
        for CSU in file.CSUs:
            if CSU not in sent_CSUs:
                CSU_payload.append({"name": CSU, "csci": sent_CSCI})
                sent_CSUs.append(CSU)

        file_to_send = {"name": file.name, "csus": file.CSUs}
        if file_to_send not in sent_files:
            sent_files.append(file_to_send)
            file_payload.append(file_to_send)

        test_result_payload.append(
            {"file": file_to_send, "test_run": test_run_id, "HLR_entry": file.hlr, "statement_coverage": file.statement,
             "decision_coverage": file.decision, "MC_DC_coverage": file.mcdc})

    return CSU_payload, file_payload, test_result_payload


def post_CSCI(CSCI, test_run_id, url):
    """
    Send a post request containing the CSCI to the database.

    :param CSCI: A CSCI that are to be sent to the database.
    :param test_run_id: The given id for the test run that included the result.
    :param url: Database url to receive the request.
    """

    # Post CSCI
    payload = {"name": CSCI.name}
    response = requests.post(url + "csci/", data=payload)
    sent_CSCI = response.json()[0]["id"]

    # Create payloads for CSU, File, Test result using CSCI
    CSU_payload, file_payload, test_result_payload = create_payloads_without_matched_id(sent_CSCI, CSCI.files,
                                                                                        test_run_id)

    # Post CSU
    response = requests.post(url + "csu/", json=CSU_payload)
    csus = response.json()

    # Post File
    file_payload = match_file_with_csu_id(file_payload, csus)
    response = requests.post(url + "file/", json=file_payload)

    # Post TestResult
    test_result_payload = match_test_result_with_file_id(response.json(), test_result_payload, csus)
    response = requests.post(url + "test_result/", json=test_result_payload)


def post_test_run(label, timestamp, summary, runner, test_run_name, url):
    """
    Send a post-request to the database containing the Test-Run data.

    :param label: Label for the test. ("trail", "peer" or "baseline")
    :param timestamp: Timestamp of when the test was run.
    :param summary: Result from the summary covsum file.
    :param runner: User who ran the test.
    :param test_run_name: Name of the test-run which included the result.
    :param url: Database url to receive the request.
    """

    # TestRun
    payload = {
        "name": test_run_name,
        "timestamp": timestamp,
        "computer": runner,
        "label": label,
        "HLR_entry": summary.hlr,
        "statement_coverage": summary.statement,
        "decision_coverage": summary.decision,
        "MC_DC_coverage": summary.mcdc
    }
    response = requests.post(url + "test_run/", data=payload)
    test_run = response.json()
    return test_run["id"]


def post(config, CSCI, summary, result, label, test_run_name):
    """
    Post test run statistics to the database.parse

    :param config: A dictionary with the user configurations.
    :param CSCI: A CSCI object.
    :param summary: A Summary object.
    :param result: A Result object.
    :param test_run_name: The name of the test run.CSCI
    :param label: The label of the test run, can be either trail, peer or baseline.
    """

    url = "http://" + config["url"] + ":" + str(config["port"]) + "/rest_api/"
    print("Posting into database...")
    test_run_id = post_test_run(label, CSCI.timestamp, summary, CSCI.runner, test_run_name, url)
    post_CSCI(CSCI, test_run_id, url)
    post_results(result, test_run_id, url)
    print("Finished posting to the database")


def parse(args):
    """
    Parse data according to the commandline arguments.

    :param args: Commandline arguments.
    :return: A list of all the items (parsed or not parsed). [dict (user configurations), CSCI, Summary, Result]
    """
    print("Parsing files...")
    CSCI, summary, config, files, result = (None,) * 5
    summary_path = Path(args.folder).joinpath(args.summary).resolve() if args.summary and args.folder else None
    CSCI_path = Path(args.folder).joinpath(args.CSCI).resolve() if args.folder and args.CSCI \
        else Path(args.CSCI).resolve() if args.CSCI \
        else None

    if args.folder:
        files = get_files(args.folder,
                          exclusions=[CSCI_path if CSCI_path else None, summary_path if summary_path else None])

    if args.config:
        config_path = Path(args.config).resolve()
        config = parse_config(config_path)

    if args.CSCI:
        CSCI = parse_CSCI(CSCI_path,
                          args.CSCI_name,
                          Path(args.config).parent.joinpath(config["CSCI_tree_path"]).resolve() if config else "")

    if files:
        result = parse_results(files)

    if args.summary:
        summary = parse_summary(summary_path)

    return config, CSCI, summary, result


def print_parsed_items(*items):
    """
    Print a list of parsed items.

    :param items: The list of items to print.
    """
    for parsed in items:
        print("")
        if parsed:
            if hasattr(parsed, "print"):
                parsed.print()
            elif hasattr(parsed, "__iter__") and not isinstance(parsed, str):
                for item in parsed:
                    if hasattr(item, "print"):
                        item.print()
                    else:
                        print(item)
            else:
                print(parsed)


def get_files(folder, exclusions=None, pattern="*.ctr"):
    """
    Get all files within a folder.

    :param folder: The absolute or relative folder path.
    :param exclusions: A list of file names to exclude.
    :param pattern: Only files that matches this pattern will be included in the list. The pattern is a string.
    :return: A list of files.
    """
    path = Path(folder).resolve()
    files = list(filter(Path.is_file, path.glob(pattern)))

    if exclusions is not None:
        for file in exclusions:
            if file in files:
                files.remove(file)

    return files


def add_common_arguments(argparser, required):
    """
    Add arguments that are common to multiple argument parsers.

    :param argparser: The argument parser to add the arguments to.
    :param required: Specifies whether certain arguments should be required or not.add
    """
    argparser.add_argument(
        "-c",
        "--CSCI",
        type=str,
        required=False,
        default="CSCI.ctr" if required else None,
        dest="CSCI",
        help="absolute or relative path to the CSCI file, "
             "defaults to 'CSCI.ctr' if 'add' is used"
    )
    argparser.add_argument(
        "-s",
        "--summary",
        type=str,
        required=False,
        default="summary.covsum" if required else None,
        dest="summary",
        help="absolute or relative path to the summary file, "
             "defaults to 'summary.covsum' if 'add' is used"
    )
    argparser.add_argument(
        "-co",
        "--config",
        type=str,
        required=required,
        dest="config",
        help="absolute or relative path to the user configuration file"
    )
    argparser.add_argument(
        "-cn",
        "--CSCI_name",
        type=str,
        required=False,
        default="General" if required else None,
        dest="CSCI_name",
        help="name of the CSCI"
    )
    argparser.add_argument(
        "-f",
        "--folder",
        type=str,
        required=required,
        dest="folder",
        help="absolute or relative path to the test folder"
    )
    argparser.add_argument(
        "-l",
        "--label",
        type=str,
        required=False,
        dest="label",
        default="trail",
        choices=["trail", "peer", "baseline"],
        help="associate test run to a specific code base"
    )
    argparser.add_argument(
        "-n",
        "--name",
        type=str,
        required=False,
        default="",
        dest="name",
        help="name the test run"
    )


def main():
    parser = construct_parser()
    args = parser.parse_args()
    data = parse(args)

    if args.command == "add":
        post(*data, args.label, args.name)
    elif args.command == "parse":
        print_parsed_items(data[0].items(), *data[1::])


if __name__ == "__main__":
    main()
