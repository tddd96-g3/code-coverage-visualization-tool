from datetime import *
from classes import File, Result, CSCI, Summary
import re
import toml
import xml.etree.ElementTree as ET

MONTHS = {"Jan": 1, "Feb": 2, "Mar": 3, "Apr": 4, "May": 5, "Jun": 6,
          "Jul": 7, "Aug": 8, "Sep": 9, "Oct": 10, "Nov": 11, "Dec": 12}


def parse_results(result_files):
    """
    Parse a list of result files generated by GoogleTest.

    :param result_files: The list of absolute paths to result files.
    :return: A list of Result objects.
    """
    res = {}  # {name: Result}
    for result_file in result_files:
        for result in parse_result(result_file):
            if result in res:
                res[result.name] += result
            else:
                res[result.name] = result

    return list(res.values())


def parse_result(result_file):
    """
    Parse a single result file generated by GoogleTest.Result

    :param result_file: The absolute path to the result file.
    :return: A list of Result objects.
    """
    res = []  # [Result]
    all_search = r"Start Test: (?P<test>[^\s]+)" \
                 r"|End Test: (?P<end_test>[^\s]+)" \
                 r"|PASSED: (?P<passed>[^\s]+)" \
                 r"|FAILED: (?P<failed>[^\s]+)"
    with open(result_file, "r") as file:
        read_file = file.read()
        test = None

        for match in re.finditer(all_search, read_file):
            if match.group("passed") is not None and test:
                test.passed += 1

            elif match.group("failed") is not None and test:
                test.failed += 1

            elif match.group("test") is not None:
                if match.group("test") in reversed(res):
                    test = next(result for result in reversed(res) if result.name == match.group("test"))
                else:
                    test = Result(match.group("test"))
                    res.append(test)
            else:
                # done parsing that test result.
                test = None
    return res


def parse_summary(summary_file):
    """
    Parse the summary file generated by Saab CovSum.

    :param summary_file: The absolute path to the summary file.
    :return: A Summary object.
    """
    with open(summary_file, "r") as file:
        percentages = re.findall(r"N/A|(\d*.?\d*)%", file.read())  # Read all percentages from the summary file
    percentages = [float(i) if i != '' else None for i in percentages]  # Convert all percentages to floats
    return Summary(*percentages)


def parse_timestamp(CSCI_file):
    """
    Parse a CSCI file for when the file was generated.

    :param CSCI_file: The absolute path to CSCI-file
    :return: Datetime of when the CSCI-file was generated.
    """
    time_search = r"Test Started:\s*\w*\s*(?P<month>\w*)\s*(?P<day>\w*)\s*(?P<hours>\d*):(?P<minutes>\d*):" \
                  r"(?P<seconds>\d*)\s*(?P<year>\d*)"
    with open(CSCI_file, "r") as file:
        match = re.search(time_search, file.read())
    timestamp = datetime(int(match.group('year')), MONTHS[match.group('month')], int(match.group('day')),
                         int(match.group('hours')), int(match.group('minutes')), int(match.group('seconds')))

    return timestamp


def parse_CSCI(CSCI_file, CSCI_name="General", CSCI_tree_path=""):
    """
    Parse and calculate the coverage statistics for each file and associate it with its components as well as units.

    :param CSCI_file: The absolute path to the CSCI file.
    :param CSCI_name: The name of the CSCI.
    :param CSCI_tree_path: The absolute path to the file for CSCI description.
    :return: A CSCI object.
    """
    timestamp = parse_timestamp(CSCI_file)
    runner = parse_runner(CSCI_file)
    file_data = parse_CSCI_data(CSCI_file)
    files = {}

    for file_name in file_data:
        files[file_name] = File(file_name, *file_data[file_name])

    try:
        CSCI_tree = get_CSCI_tree(CSCI_tree_path)
        for CSU in CSCI_tree.findall("csu"):
            CSU_name = CSU.attrib["name"]
            for file in CSU.findall("source"):
                file_name = file.attrib["filename_rel"].split("/")[-1]
                if file_name in files:
                    files[file_name].add_CSU(CSU_name)
    except FileNotFoundError:
        pass
    return CSCI(CSCI_name, list(files.values()), timestamp, runner)


def get_CSCI_tree(CSCI_tree_path):
    """
    Parse the description file for the CSCI.

    :param CSCI_tree_path: The absolute path to the file for CSCI description.
    :return: An XML element tree.
    """
    return ET.parse(CSCI_tree_path)


def parse_CSCI_data(CSCI_file):
    """
    Parse the CSCI file generated by Cantata++.

    :param CSCI_file: The absolute path to the CSCI file.
    :return: A dictionary of files and their statistics. {file_name: (HLR, Statement, Decision, MC/DC)}.
    """
    res = {}
    file_search = r"-(?:\s)(?P<file_name>(?:[^\s]|[\r\n])*\.[ch](?:pp)?)" \
                  r"|entry point *\d* *(?P<entry>\d*.\d*)%.*[\r\n|\n]" \
                  r"statement  *\d* *(?P<statement>\d*.\d*)%.*[\r\n|\n]" \
                  r"decision *\d* *(?P<decision>\d*.\d*)%.*[\r\n|\n]" \
                  r"masking eff. *\d* *(?P<masking>\d*.\d*)%"

    with open(CSCI_file, "r") as file:
        name = None

        for match in re.finditer(file_search, file.read()):
            if match.group("file_name") is not None and name is None:
                name = match.group("file_name").replace("\n", "")
                if name not in res:
                    res[name] = 0, 0, 0, 0, 0
            elif match.group("file_name") is None and name in res:
                entry, statement, decision, masking, i = res[name]
                res[name] = entry + float(match.group("entry")), \
                            statement + float(match.group("statement")), \
                            decision + float(match.group("decision")), \
                            masking + float(match.group("masking")), \
                            i + 1
                name = None

    for key, value in res.items():
        entry, statement, decision, masking, i = value
        res[key] = round(entry / i, 1), round(statement / i, 1), round(decision / i, 1), round(masking / i, 1)
    return res


def parse_runner(CSCI_file):
    """
    Parse a CSCI file for which system ran the test.

    :param CSCI_file: The absolute path to CSCI file.
    :return: The name of the system.
    """
    runner_search = r'executed by =\s*(?P<runner>\S*)'
    match = None
    with open(CSCI_file, "r") as file:
        match = re.search(runner_search, file.read())

    return match.group("runner")


def parse_config(config_file):
    """
    Parse the configuration file necessary for establishing a connection with the database.

    :param config_file: The absolute path to the configuration file.
    :return: A dictionary with all the information contained in the configuration file.
    """
    return toml.load(config_file)

