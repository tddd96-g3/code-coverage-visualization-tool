import { createApp } from "vue";
import App from "./App.vue";
import "./styles/index.css";
import VueApexCharts from "vue3-apexcharts";

import router from "./router/index";

const app = createApp(App);

app.use(VueApexCharts);
app.use(router);

// needs to be last
app.mount("#app");
