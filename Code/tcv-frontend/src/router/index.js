import { createRouter, createWebHistory } from "vue-router";
import Summary from "@/views/Summary";
import Unit from "@/views/CSCI";
import Search from "@/views/Search";
import Component from "@/views/CSU";
import TestRun from "@/views/TestRun";
import Faq from "@/views/Faq"

const routes = [
  {
    path: "/",
    name: "Summary",
    component: Summary,
  },
  {
    path: "/search",
    component: Search,
  },
  {
    path: "/csci/:id",
    component: Unit,
  },
  {
    path: "/csu/:id",
    component: Component,
  },
  {
    path: "/test_run/:id",
    component: TestRun,
  },
  {
    path: "/test_run/:testRunId/csu/:id",
    component: Component,
  },
  {
    path: "/faq",
    component: Faq,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
