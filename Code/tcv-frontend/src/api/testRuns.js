import { ref, onMounted, computed } from "vue";

const testRuns = ref([]);
export function useAllTestRuns(number = undefined) {
  const getTestRuns = async () => {
    const res = await fetch(
      `${process.env.VUE_APP_API_ENDPOINT}/test_run${
        number ? `/?recent=${number}` : ""
      }`,
      { mode: "cors" }
    );
    testRuns.value = await res.json();
  };

  onMounted(getTestRuns);

  return {
    testRuns,
    getTestRuns,
  };
}


// TODO: use useAllTestRuns with param 1 instead
export function useLatestTestRun(testRuns) {
  const latestTestRun = computed(
    () => testRuns.value[testRuns.value.length - 1]
  );

  return {
    latestTestRun,
  };
}

export function useTestRun(id) {
  const testRun = ref(undefined)
  const getTestRun = async () => {
    const res = await fetch(
      `${process.env.VUE_APP_API_ENDPOINT}/test_run/${id}`,
      { mode: "cors" }
    );
    testRun.value = await res.json()
  }

  onMounted(getTestRun)

  return {
    testRun
  }
}

export function useParseTestRuns(testRuns) {
  let codeCoverage = { hlr: [], statement: [], decision: [], mcdc: [] };

  const parsedTestRuns = computed(() => {
    const data = testRuns.value;
    for (let i = 0; i < data.length; i += 1) {
      codeCoverage.hlr.push([data[i].timestamp, parseFloat(data[i].HLR_entry)]);
      codeCoverage.statement.push([
        data[i].timestamp,
        parseFloat(data[i].statement_coverage),
      ]);
      codeCoverage.decision.push([
        data[i].timestamp,
        parseFloat(data[i].decision_coverage),
      ]);
      codeCoverage.mcdc.push([
        data[i].timestamp,
        parseFloat(data[i].MC_DC_coverage),
      ]);
    }
    return codeCoverage;
  });

  return {
    parsedTestRuns,
  };
}
