/* eslint-disable no-unused-vars */
import { ref, onMounted, computed } from "vue";

export function useUnitFiles(unit_id) {
  let files = ref(undefined);

  const getUnitFiles = async () => {
    const res = await fetch(
      `${process.env.VUE_APP_API_ENDPOINT}/file/?csci=${unit_id}`,
      { mode: "cors" }
    );
    files.value = await res.json();
  };

  onMounted(getUnitFiles);

  return {
    files,
  };
}

export function useComponentFiles(id, testRunId = undefined) {
  let files = ref(undefined);

  const getComponentFiles = async () => {
    const res = await fetch(
      `${process.env.VUE_APP_API_ENDPOINT}${
        testRunId
          ? `/file/?csu=${id}&test_run=${testRunId}`
          : `/file/?csu=${id}`
      }`,
      {
        mode: "cors",
      }
    );
    files.value = await res.json();
  };

  onMounted(getComponentFiles);

  return {
    files,
  };
}

export function useParsedComponents(components) {
  const parsedComponents = computed(() =>
    components.value.map((entry) => {
      return {
        id: entry.id,
        name: entry.name,
        unit: entry.unit,
        coverage:
          (
            (parseInt(entry.HLR_entry) +
              parseInt(entry.statement_coverage) +
              parseInt(entry.decision_coverage) +
              parseInt(entry.MC_DC_coverage)) /
            4
          ).toString() + "%",
      };
    })
  );

  return { parsedComponents };
}

export function useTestRunFiles(id) {
  const files = ref(undefined);

  const getFiles = async () => {
    const res = await fetch(
      `${process.env.VUE_APP_API_ENDPOINT}/file/?test_run=${id}`,
      {
        mode: "cors",
      }
    );
    files.value = await res.json();
  };

  onMounted(getFiles);

  return {
    files,
  };
}
