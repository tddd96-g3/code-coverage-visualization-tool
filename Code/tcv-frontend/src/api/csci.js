import { ref, onMounted, computed } from "vue";

export const units = ref([]);

export function useAllUnits() {
  const getUnits = async () => {
    const res = await fetch(`${process.env.VUE_APP_API_ENDPOINT}/csci`, {
      mode: "cors",
    });
    units.value = await res.json();
  };

  onMounted(getUnits);

  return {
    units,
    getUnits,
  };
}

export function useParsedUnits(units) {
  const parsedUnits = computed(() =>
    units.value.map((unit) => {
      return {
        title: unit.name,
        link: `${unit.id}`,
      };
    })
  );

  return { parsedUnits };
}

export function useUnit(id) {
  const unit = ref(undefined);
  const getUnit = async () => {
    const res = await fetch(`${process.env.VUE_APP_API_ENDPOINT}/csci/${id}`, {
      mode: "cors",
    });
    unit.value = await res.json();
  };

  onMounted(getUnit);

  return {
    unit,
  };
}
