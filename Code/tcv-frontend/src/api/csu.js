/* eslint-disable no-unused-vars */
import { ref, onMounted, computed } from "vue";

export function useUnitComponents(unit_id) {
  const components = ref(undefined);
  const getUnitComponents = async () => {
    const res = await fetch(
      `${process.env.VUE_APP_API_ENDPOINT}/csu/?csci=${unit_id}`,
      { mode: "cors" }
    );
    const data = await res.json();

    components.value = data.map((entry) => {
      return {
        id: entry.id,
        name: entry.name,
        unit: entry.unit,
        coverage:
          (
            (parseInt(entry.HLR_entry) +
              parseInt(entry.statement_coverage) +
              parseInt(entry.decision_coverage) +
              parseInt(entry.MC_DC_coverage)) /
            4
          ).toString() + "%",
      };
    });
  };

  onMounted(getUnitComponents);

  return {
    components,
  };
}

export function useComponent(id) {
  const component = ref(undefined);
  const getComponent = async () => {
    const res = await fetch(`${process.env.VUE_APP_API_ENDPOINT}/csu/${id}`, {
      mode: "cors",
    });
    component.value = await res.json();
  };

  onMounted(getComponent);

  return { component };
}

export function useTestRunCSU(id) {
  const CSU = ref(undefined);

  const getCSUs = async () => {
    const res = await fetch(
      `${process.env.VUE_APP_API_ENDPOINT}/csu/?test_run=${id}`,
      {
        mode: "cors",
      }
    );
    const data = await res.json();

    CSU.value = data.map((entry) => {
      return {
        id: entry.id,
        name: entry.name,
        unit: entry.unit,
        coverage:
          (
            (parseInt(entry.HLR_entry) +
              parseInt(entry.statement_coverage) +
              parseInt(entry.decision_coverage) +
              parseInt(entry.MC_DC_coverage)) /
            4
          ).toString() + "%",
      };
    });
  };

  onMounted(getCSUs);

  return {
    CSU,
  };
}
