import { ref, onMounted } from "vue";

export function useTests(id) {
  const tests = ref(undefined)
  const getTests = async () => {
    const res = await fetch(
      `${process.env.VUE_APP_API_ENDPOINT}/test/?test_run=${id}`,
      { mode: "cors" }
    );
    const data = await res.json()
    
    tests.value = {
      passed: data.map((entry) => entry.passed).reduce((a, b) => a + b),
      failed: data.map((entry) => entry.failed).reduce((a, b) => a + b)
    }
  }

  onMounted(getTests)

  return {
    tests
  }
}