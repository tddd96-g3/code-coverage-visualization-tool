# tcv-frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Style Guide
```
The project is trying to follow the [Vue Style Guide](https://vuejs.org/v2/style-guide/#Component-style-scoping-essential)
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Colors
#F8B749
#D97704
#15427B
#004C97
#DA291C
#34322E
#5F625F
#A49786
#D9D9D6
#FFFFFF
#0499D9