from rest_framework import routers
from .rest_api import *
from django.urls import path, include

router = routers.DefaultRouter()
router.register(r"rest_api/csci", CSCIViewSet, "csci")
router.register(r"rest_api/csu", CSUViewSet, "csu")
router.register(r"rest_api/file", FileViewSet, "file")
router.register(r"rest_api/test_result", TestResultViewSet, "test_result")
router.register(r"rest_api/test_run", TestRunViewSet, "test_run")
router.register(r"rest_api/test", TestViewSet, "test")

urlpatterns = [
    path("", include((router.urls, "api"), namespace = "rest_api"))
]


