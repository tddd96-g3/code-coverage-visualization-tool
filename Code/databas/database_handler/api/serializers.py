from rest_framework import serializers
from api.models import *

# api Serializer
class CSCISerializer(serializers.ModelSerializer):
    class Meta:
        model = CSCI
        fields = "__all__"
    
class CSCICoverageSerializer(serializers.ModelSerializer):
    class Meta:
        model = CSCI
        fields = "__all__"
    HLR_entry = serializers.DecimalField(max_digits=5, decimal_places=2)
    statement_coverage = serializers.DecimalField(max_digits=5, decimal_places=2)
    decision_coverage = serializers.DecimalField(max_digits=5,decimal_places=2)
    MC_DC_coverage = serializers.DecimalField(max_digits=5,decimal_places=2)

class CSUSerializer(serializers.ModelSerializer):
    class Meta:
        model = CSU
        fields = "__all__"

class CSUCoverageSerializer(serializers.ModelSerializer):
    class Meta:
        model = CSU
        fields = "__all__"
    HLR_entry = serializers.DecimalField(max_digits=5, decimal_places=2)
    statement_coverage = serializers.DecimalField(max_digits=5, decimal_places=2)
    decision_coverage = serializers.DecimalField(max_digits=5,decimal_places=2)
    MC_DC_coverage = serializers.DecimalField(max_digits=5,decimal_places=2)

class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = "__all__"

class FileCoverageSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = "__all__"
    HLR_entry = serializers.DecimalField(max_digits=5, decimal_places=2)
    statement_coverage = serializers.DecimalField(max_digits=5, decimal_places=2)
    decision_coverage = serializers.DecimalField(max_digits=5,decimal_places=2)
    MC_DC_coverage = serializers.DecimalField(max_digits=5,decimal_places=2)


class TestRunSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestRun
        fields = "__all__"

class TestResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestResult
        fields = "__all__"

class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Test
        fields = "__all__"