from api.models import *
from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from .serializers import *
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from datetime import datetime


class CSCIViewSet(viewsets.ModelViewSet):
    serializer_class = CSCISerializer
    queryset = CSCI.objects.all()

    def calculate_coverage(self, csci, test_runs):

        HLR_entry = 0
        statement_coverage = 0
        decision_coverage = 0
        MC_DC_coverage = 0
        length = 0
        csus = CSU.objects.filter(csci_id=csci.pk)
        file_query = File.objects.none()

        for csu in csus:
            csufiles = File.objects.filter(pk__in=csu.file_set.all())
            file_query = file_query.union(csufiles)

        testresults = TestResult.objects.filter(file_id__in=file_query.values("id"))
        if test_runs:
            testresults = testresults.filter(test_run_id__in=test_runs)
        length = len(testresults)

        for testresult in testresults:
            HLR_entry += testresult.HLR_entry
            statement_coverage += testresult.statement_coverage
            decision_coverage += testresult.decision_coverage
            MC_DC_coverage += testresult.MC_DC_coverage

        if length:
            HLR_entry = HLR_entry/length
            statement_coverage = statement_coverage/length
            decision_coverage = decision_coverage/length
            MC_DC_coverage = MC_DC_coverage/length

        elif test_runs:
            return None

        payload = {"id": csci.id, "name": csci.name,
                   "HLR_entry": HLR_entry,
                   "statement_coverage": statement_coverage,
                   "decision_coverage": decision_coverage,
                   "MC_DC_coverage": MC_DC_coverage}
        return payload


    def retrieve(self, request, pk=None):

        test_runs = request.query_params.getlist("test_run")
        queryset = CSCI.objects.all()
        csci = get_object_or_404(queryset, pk=pk)

        payload = self.calculate_coverage(csci, test_runs)
        if payload is None:
            response = {"CSCI not found": "CSCI isn't connected to given Test run"}
            return Response( response, status.HTTP_404_NOT_FOUND)
        serializer = CSCICoverageSerializer(payload)
        return Response(serializer.data)


    def list(self, request):

        test_runs = request.query_params.getlist("test_run")

        queryset = CSCI.objects.all()
        csci_name = request.query_params.getlist("name")
        if csci_name:
            queryset = queryset.filter(name__in=csci_name)

        cscis = [self.calculate_coverage(csci, test_runs) for csci in queryset]
        cscis = list(filter(None, cscis))
        serializer = CSCICoverageSerializer(cscis, many=True)
        return Response(serializer.data)


    def create(self, request):

        requested_cscis = request.data if isinstance(request.data, list) else [request.data]
        cscis_to_send = []
        
        for csci in requested_cscis:

            created_csci, _ = CSCI.objects.get_or_create(name=csci['name'])
            cscis_to_send.append(created_csci)
 
        serializer = CSCISerializer(cscis_to_send, many=True)
        return Response(serializer.data)

    permission_classes = [
        permissions.AllowAny
    ]


class CSUViewSet(viewsets.ModelViewSet):

    serializer_class = CSUSerializer
    queryset = CSU.objects.all()

    def calculate_coverage(self, csu, test_runs):

        HLR_entry = 0
        statement_coverage = 0
        decision_coverage = 0
        MC_DC_coverage = 0
        length = 0

        file_query = File.objects.filter(pk__in=csu.file_set.all())
        testresults = TestResult.objects.filter(file_id__in=file_query.values("id"))
        if test_runs:
            testresults = testresults.filter(test_run_id__in=test_runs)
        length = len(testresults)

        for testresult in testresults:
            HLR_entry += testresult.HLR_entry
            statement_coverage += testresult.statement_coverage
            decision_coverage += testresult.decision_coverage
            MC_DC_coverage += testresult.MC_DC_coverage

        if length:
            HLR_entry = HLR_entry/length
            statement_coverage = statement_coverage/length
            decision_coverage = decision_coverage/length
            MC_DC_coverage = MC_DC_coverage/length

        elif test_runs:
            return None

        payload = {"id": csu.id,
                   "name": csu.name,
                   "csci": csu.csci,
                   "HLR_entry": HLR_entry,
                   "statement_coverage": statement_coverage,
                   "decision_coverage": decision_coverage,
                   "MC_DC_coverage": MC_DC_coverage}

        return payload

    def retrieve(self, request, pk=None):

        queryset = CSU.objects.all()
        test_runs = request.query_params.getlist("test_run")
        csu = get_object_or_404(queryset, pk=pk)

        payload = self.calculate_coverage(csu, test_runs)
        if payload is None:
            response = {"CSU not found": "CSU isn't connected to given Test run"}
            return Response( response, status.HTTP_404_NOT_FOUND)
        serializer = CSUCoverageSerializer(payload)
        return Response(serializer.data)

    def list(self, request):

        queryset = CSU.objects.all()
        test_runs = request.query_params.getlist("test_run")
        csu_name = request.query_params.get("name")

        if csu_name is not None:
            queryset = queryset.filter(name=csu_name)

        csci_id = request.query_params.get("csci")
        if csci_id is not None:
            queryset = queryset.filter(csci_id=csci_id)

        csus = [self.calculate_coverage(csu, test_runs) for csu in queryset]
        csus = list(filter(None, csus))
        serializer = CSUCoverageSerializer(csus, many=True)

        return Response(serializer.data)

    def create(self, request):

        requested_csus = request.data if isinstance(request.data, list) else [request.data]
        csus_to_send = []
        
        for csu in requested_csus:

            created_csu, created = CSU.objects.get_or_create(csci_id=csu['csci'], name=csu['name'])
            csus_to_send.append(created_csu)

        serializer = CSUSerializer(csus_to_send, many=True)
        return Response(serializer.data)


    permission_classes = [
        permissions.AllowAny
    ]

class FileViewSet(viewsets.ModelViewSet):

    serializer_class = FileSerializer
    queryset = File.objects.all()

    def calculate_coverage(self, file, test_runs):

        testresults = TestResult.objects.filter(file_id=file.pk)
        if test_runs:
            testresults = testresults.filter(test_run_id__in=test_runs)
        HLR_entry = 0
        statement_coverage = 0
        decision_coverage = 0
        MC_DC_coverage = 0

        for testresult in testresults:
 
            HLR_entry += testresult.HLR_entry
            statement_coverage += testresult.statement_coverage
            decision_coverage += testresult.decision_coverage
            MC_DC_coverage += testresult.MC_DC_coverage

        if len(testresults) != 0:

            HLR_entry = HLR_entry/len(testresults)
            statement_coverage = statement_coverage/len(testresults)
            decision_coverage = decision_coverage/len(testresults)
            MC_DC_coverage = MC_DC_coverage/len(testresults)
        elif test_runs:
            return None

        payload = {"id": file.id,"name": file.name,
                   "csus": file.csus ,
                   "HLR_entry": HLR_entry,
                   "statement_coverage": statement_coverage,
                   "decision_coverage": decision_coverage,
                   "MC_DC_coverage": MC_DC_coverage}
        return payload

    def retrieve(self, request, pk=None):

        queryset = File.objects.all()
        file = get_object_or_404(queryset, pk=pk)
        test_runs = request.query_params.getlist("test_run")
        payload = self.calculate_coverage(file, test_runs)
        if payload is None:
            response = {"File not found": "File isn't connected to given Test run"}
            return Response( response, status.HTTP_404_NOT_FOUND)

        serializer = FileCoverageSerializer(payload)
        return Response(serializer.data)

    def list(self, request):

        queryset = File.objects.all()
        file_name = request.query_params.get("name")
        test_runs = request.query_params.getlist("test_run")
        if file_name is not None:
            queryset = queryset.filter(name=file_name)

        csu_id = request.query_params.get("csu")
        if csu_id is not None:
            queryset = queryset.filter( csus__pk__contains=csu_id )

        csci_id = request.query_params.get("csci")
        if csci_id is not None:
            csu_query = CSU.objects.filter(csci_id=csci_id)

            file_query=File.objects.none()
            for csu in csu_query.values():
                file_query = file_query.union( queryset.filter(csus__pk__contains=csu.get("id")) )

            queryset = file_query

        files = [self.calculate_coverage(file, test_runs) for file in queryset]
        files = list(filter(None, files))

        serializer = FileCoverageSerializer(files, many=True)
        return Response(serializer.data)

    def create(self, request):

        requested_files = request.data if isinstance(request.data, list) else [request.data]

        files_to_send = []
        for file in requested_files:

            files = File.objects.filter(name=file["name"])
            queryset = File.objects.none()

            for csu_id in file['csus']:
                queryset = queryset.union( files.filter(csus__pk__contains=csu_id) )
        
            created_file, created = queryset.get_or_create(name=file['name'])
            files_to_send.append(created_file)
            if created:
                for csu_id in file['csus']:
                    created_file.csus.add(csu_id)

        serializer = FileSerializer(files_to_send, many=True)
        return Response(serializer.data)

    permission_classes = [
        permissions.AllowAny
    ]


class TestRunViewSet(viewsets.ModelViewSet):
    serializer_class = TestRunSerializer
    queryset = TestRun.objects.all()

    def list(self, request):

        queryset = TestRun.objects.all()

        testrun_name = request.query_params.getlist("name")
        if testrun_name:
            queryset = queryset.filter(name__in=testrun_name)

        testrun_label = request.query_params.getlist("label")
        if testrun_label:
            queryset = queryset.filter(label__in=testrun_label)

        testrun_computer = request.query_params.getlist("computer")
        if testrun_computer:
            queryset = queryset.filter(computer__in=testrun_computer)

        testrun_timestamp = request.query_params.get("timestamp")
        if testrun_timestamp is not None:
            date = datetime.strptime(testrun_timestamp,"%Y-%m-%d").date()
            queryset = queryset.filter(timestamp__date = date)
            
        csci_id = request.query_params.getlist("csci")
        if csci_id:
            csu_query = CSU.objects.filter(csci_id__in=csci_id)
            all_files_query = File.objects.all()
            file_query = File.objects.none()

            for csu in csu_query.values():
                file_query = file_query.union( all_files_query.filter(csus__pk__contains=csu.get("id")) )

            test_result_query = TestResult.objects.filter(file_id__in=file_query.values("id"))

            queryset = queryset.filter(pk__in=test_result_query.values("test_run_id"))
    
        queryset = queryset.order_by('-timestamp')

        recent = request.query_params.get("recent")
        if recent is not None:
            recent = int(recent)
            if recent >= 0:
                if recent < queryset.count():
                    queryset = queryset[:recent]
        
        serializer = TestRunSerializer(queryset, many=True)
        return Response(serializer.data)


    permission_classes = [
        permissions.AllowAny
    ]


class TestResultViewSet(viewsets.ModelViewSet):

    serializer_class = TestResultSerializer
    queryset = TestResult.objects.all()

    def create(self, request):

        serializer = self.get_serializer(data=request.data, many=isinstance(request.data, list))
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        return Response(serializer.data)

    permission_classes = [
        permissions.AllowAny
    ]
    

class TestViewSet(viewsets.ModelViewSet):

    serializer_class = TestSerializer
    queryset = Test.objects.all()

    def create(self, request):

        requested_tests = request.data if isinstance(request.data, list) else [request.data]
        tests_to_send = []
        
        for test in requested_tests:

            created_test, _ = Test.objects.get_or_create(name=test['name'],
                                                         test_run_id=test['test_run'],
                                                         passed=test['passed'],
                                                         failed=test['failed'])
            tests_to_send.append(created_test)
 
        serializer = TestSerializer(tests_to_send, many=True)
        return Response(serializer.data)

    def list(self, request):

        queryset = Test.objects.all()
        tests = request.query_params.getlist("test_run")

        if tests:
            queryset = queryset.filter(test_run__in=tests)
        
        serializer = TestSerializer(queryset, many=True)
        return Response(serializer.data)

    permission_classes = [
        permissions.AllowAny
    ]
