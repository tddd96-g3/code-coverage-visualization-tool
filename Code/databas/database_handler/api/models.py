from django.db import models
from django.core.exceptions import ObjectDoesNotExist

# signals imports
from django.dispatch import receiver

from django.db.models.signals import (
    pre_delete,
    post_delete
)

class CSCI(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __repr__(self):
        return "<CSCI: %s>" % self.name

class CSU(models.Model):
    name = models.CharField(max_length=255)
    csci = models.ForeignKey("CSCI", on_delete=models.CASCADE)

    def __repr__(self):
        return "<CSU: %s>" % self.name

@receiver(pre_delete, sender=CSU)
def csu_pre_delete(sender, instance, *args, **kwargs):

    queryset = File.objects.filter(csus__pk__contains=instance.id)
    for file in queryset:
        file.csus.remove(instance)
        
        if file.csus.count() == 0:
            file.delete()

@receiver(post_delete, sender=CSU)
def csu_post_delete(sender, instance, *args, **kwargs):

    try:
        queryset = CSU.objects.filter(csci=instance.csci)
        if not queryset:
            instance.csci.delete()
    except ObjectDoesNotExist:
        pass

class File(models.Model):
    name = models.CharField(max_length=255)
    csus = models.ManyToManyField(CSU)

    def __repr__(self):
        return "<File: %s>" % self.name

@receiver(pre_delete,  sender=File)
def file_pre_delete(sender, instance, *args, **kwargs):

    for csu in instance.csus.all():
        queryset = File.objects.filter(csus__pk__contains=csu.id)

        if queryset.count() == 1:
            csu.delete()


class TestRun(models.Model):
    name = models.CharField(max_length=255, blank=True)

    timestamp = models.DateTimeField()
    computer = models.CharField(max_length=255, blank=True)
    label_choices = ( ("trail", "trail"), ("baseline", "baseline"), ("peer", "peer"), ("",""))
    label = models.CharField(max_length=15, choices = label_choices, default = "", blank=True)

    HLR_entry = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    statement_coverage = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    decision_coverage = models.DecimalField(max_digits=5,decimal_places=2, null=True)
    MC_DC_coverage = models.DecimalField(max_digits=5,decimal_places=2, null=True)

    def __repr__(self):
        return "<TestRun: %s>" % self.name

class TestResult(models.Model):

    file = models.ForeignKey("File", on_delete=models.CASCADE, null=True )
    test_run = models.ForeignKey("TestRun", default = 1, on_delete=models.CASCADE)

    
    HLR_entry = models.DecimalField(max_digits=5, decimal_places=2)
    statement_coverage = models.DecimalField(max_digits=5, decimal_places=2)
    decision_coverage = models.DecimalField(max_digits=5,decimal_places=2)
    MC_DC_coverage = models.DecimalField(max_digits=5,decimal_places=2)
    
    def __repr__(self):
        return "<TestResult: %s>" % "TestResult Object " + str(self.id)



@receiver(post_delete, sender=TestResult)
def test_result_post_delete(sender, instance, *args, **kwargs):

    try:
        queryset = TestResult.objects.filter(file=instance.file)
        if not queryset:
            instance.file.delete()
    except ObjectDoesNotExist:
        pass


class Test(models.Model):
    name = models.CharField(max_length=256)
    test_run = models.ForeignKey("TestRun", default=1, on_delete=models.CASCADE)

    passed = models.IntegerField()
    failed = models.IntegerField()

    def __repr__(self):
        return "<Test: %s>" % self.name



